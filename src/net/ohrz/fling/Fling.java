/*
 * Copyright (C) 2013 XuiMod
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ohrz.fling;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.callbacks.XC_InitPackageResources.InitPackageResourcesParam;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;
import net.ohrz.fling.mods.AnimationControlsMod;
import net.ohrz.fling.mods.BatteryBarMod;
import net.ohrz.fling.mods.ClassicRecentsMod;
import net.ohrz.fling.mods.EdgeEffectMod;
import net.ohrz.fling.mods.InputMethodAnimationMod;
import net.ohrz.fling.mods.ListViewAnimationMod;
import net.ohrz.fling.mods.LockscreenTorchMod;
import net.ohrz.fling.mods.LockscreenVolumeMod;
import net.ohrz.fling.mods.RandomQuickSettingsColorMod;
import net.ohrz.fling.mods.ScrollerMod;
import net.ohrz.fling.mods.SecondsClockMod;
import net.ohrz.fling.mods.SystemAnimationMod;
import net.ohrz.fling.mods.TickerAnimation;
import net.ohrz.fling.mods.ToastAnimationMod;
import net.ohrz.fling.mods.VolumePanelMod;

public class Fling implements IXposedHookZygoteInit,IXposedHookLoadPackage,IXposedHookInitPackageResources{
	
	public static String MODULE_PATH = null;
	public static XSharedPreferences pref;
	
	@Override
	public void initZygote(StartupParam startupParam) throws Throwable {
		MODULE_PATH = startupParam.modulePath;		
		pref = new XSharedPreferences(Common.MY_PACKAGE_NAME);
		AnimationControlsMod.initZygote();
	}
	
	@Override
	public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {		
		pref.reload();
		SecondsClockMod.handleLoadPackage(lpparam);
		BatteryBarMod.handleLoadPackage(lpparam,pref);
		LockscreenVolumeMod.handleLoadPackage(lpparam,pref);
		ListViewAnimationMod.handleLoadPackage(pref);
		VolumePanelMod.handleLoadPackage(lpparam,pref);
		LockscreenTorchMod.handleLoadPackage(lpparam,pref);
		AnimationControlsMod.handleLoadPackage(lpparam);
		InputMethodAnimationMod.handleLoadPackage(lpparam,pref);
		ToastAnimationMod.handleLoadPackage(lpparam,pref);
		RandomQuickSettingsColorMod.loadPackage(lpparam,pref);
		ClassicRecentsMod.loadPackage(lpparam,pref);
		ScrollerMod.handleLoadPackage(pref);
		EdgeEffectMod.handleLoadPackage(lpparam, pref);
		TickerAnimation.handleLoadPackage(lpparam, pref);
	}

	@Override
	public void handleInitPackageResources(InitPackageResourcesParam resparam) throws Throwable {
		pref.reload();
		SystemAnimationMod.handleInitPackageResources(pref, resparam);
	}
	
}
